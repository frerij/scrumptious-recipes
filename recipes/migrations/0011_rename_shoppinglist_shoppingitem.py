# Generated by Django 4.0.3 on 2022-03-18 00:27

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('recipes', '0010_rename_shoppingitem_shoppinglist'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ShoppingList',
            new_name='ShoppingItem',
        ),
    ]
