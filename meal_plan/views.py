from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from meal_plan.models import MealPlan

# Create your views here.


class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plan/list.html"
    paginate_by = 3

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanDetailView(DetailView):
    model = MealPlan
    template_name = "meal_plan/detail.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plan/new.html"
    fields = ["name", "recipes", "date"]

    def form_valid(self, form):
        # save meal plan but dont put in database
        plan = form.save(commit=False)

        # assign owner to meal plan
        plan.owner = self.request.user

        # now, save to database
        plan.save()

        # save all many to many relationshops
        form.save_m2m()

        # redirect to detail page for the meal plan
        return redirect("meal_plan_detail", pk=plan.id)


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plan/edit.html"
    fields = ["name", "date", "recipes"]

    def get_success_url(self) -> str:
        return reverse_lazy("meal_plan_detail", args=[self.object.id])


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plan/delete.html"
    success_url = reverse_lazy("meal_plan_list")
