from django.urls import path
from meal_plan.views import (
    MealPlanCreateView,
    MealPlanDeleteView,
    MealPlanDetailView,
    MealPlanListView,
    MealPlanUpdateView,
)

urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plan_list"),
    path(
        "<int:pk>/detail/",
        MealPlanDetailView.as_view(),
        name="meal_plan_detail",
    ),
    path(
        "new/",
        MealPlanCreateView.as_view(),
        name="meal_plan_create",
    ),
    path("<int:pk>/edit/", MealPlanUpdateView.as_view(), name="meal_plan_edit"),
    path(
        "<int:pk>/delete/",
        MealPlanDeleteView.as_view(),
        name="meal_plan_delete",
    ),
]
